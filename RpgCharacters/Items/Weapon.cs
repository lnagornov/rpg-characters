using RpgCharacters.Characters;

namespace RpgCharacters.Items;

public class Weapon : Item
{
    public int Damage { get; private set; }
    public double AttackSpeed { get; private set; }
    public WeaponType Type { get; private set; }
    
    public Weapon(string name, int levelRequired, SlotType slot, WeaponType type, int damage, double attackSpeed)
    {
        Name = name;
        LevelRequired = levelRequired;
        Slot = slot;
        Type = type;
        Damage = damage;
        AttackSpeed = attackSpeed;
    }
    
    /// <summary>
    /// Calculates the weapon damage per second, by multiplying damage and attacks per second.
    /// </summary>
    /// <returns>Damage per second of the weapon</returns>
    public double GetDamagePerSecond()
    {
        return Math.Round(Damage * AttackSpeed, 2);
    }
}