using RpgCharacters.Characters;

namespace RpgCharacters.Items;

public abstract class Item
{
    public string Name { get; init; } = "Item";
    public int LevelRequired { get; protected init; } = 1;
    public SlotType Slot { get; protected init; }
}