namespace RpgCharacters.Items;

public enum ArmorType
{
    Cloth,
    Leather,
    Mail,
    Plate
}