using RpgCharacters.Attributes;
using RpgCharacters.Characters;

namespace RpgCharacters.Items;

public class Armor : Item
{
    public ArmorType Type { get; set; }
    public PrimaryAttributes Attributes { get; set; }
    public Armor(string name, int levelRequired, SlotType slot, ArmorType type, PrimaryAttributes attributes)
    {
        Name = name;
        LevelRequired = levelRequired;
        Slot = slot;
        Type = type;
        Attributes = attributes;
    }
}