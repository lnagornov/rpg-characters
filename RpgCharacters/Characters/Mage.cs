using RpgCharacters.Attributes;
using RpgCharacters.Items;

namespace RpgCharacters.Characters;

public class Mage : Character
{
    public Mage()
    {
        Type = CharacterType.Mage;
        // Set Base and Total attributes
        BasePrimaryAttributes = PrimaryAttributesFactory.GetBaseAttributes(Type);
        // Set allowed Weapon and Armor types
        AllowedArmorTypes.Add(ArmorType.Cloth);
        AllowedWeaponTypes.AddRange(new List<WeaponType>()
        {
            WeaponType.Staff,
            WeaponType.Wand
        });
    }
    
    public override double CalculateDamage()
    {
        Weapon? equippedWeapon = (Weapon?) Equipment[SlotType.Weapon];
        double damagePerSecond = equippedWeapon?.GetDamagePerSecond() ?? BaseDamage;

        return damagePerSecond * (1 + (double) TotalPrimaryAttributes.Intelligence / 100);
    }
}