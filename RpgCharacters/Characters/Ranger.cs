using RpgCharacters.Attributes;
using RpgCharacters.Items;

namespace RpgCharacters.Characters;

public class Ranger : Character
{
    public Ranger()
    {
        Type = CharacterType.Ranger;
        // Set Base attributes
        BasePrimaryAttributes = PrimaryAttributesFactory.GetBaseAttributes(Type);
        // Set allowed Weapon and Armor types
        AllowedArmorTypes.AddRange(new List<ArmorType>()
        {
            ArmorType.Leather, 
            ArmorType.Mail
        });
        AllowedWeaponTypes.Add(WeaponType.Bow);
    }
    
    public override double CalculateDamage()
    {
        Weapon? equippedWeapon = (Weapon?) Equipment[SlotType.Weapon];
        double damagePerSecond = equippedWeapon?.GetDamagePerSecond() ?? BaseDamage;
        
        return damagePerSecond * (1 + (double) TotalPrimaryAttributes.Dexterity / 100);
    }
}