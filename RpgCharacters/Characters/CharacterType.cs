namespace RpgCharacters.Characters;

public enum CharacterType
{
    Mage,
    Ranger,
    Rogue,
    Warrior
}