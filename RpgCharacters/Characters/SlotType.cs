namespace RpgCharacters.Characters;

public enum SlotType
{
    Head,
    Body,
    Legs,
    Weapon
}