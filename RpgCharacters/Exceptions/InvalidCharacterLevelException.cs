namespace RpgCharacters.Exceptions;

public class InvalidCharacterLevelException : Exception
{
    public InvalidCharacterLevelException(string message) : base(message)
    {
    }

    public override string Message => "Invalid character level.";
}