namespace RpgCharacters.Exceptions;

public class InvalidArmorException : Exception
{
    public InvalidArmorException(string message) : base(message)
    {
    }

    public override string Message => "Invalid armor type. This character can't use it.";
}