namespace RpgCharacters.Exceptions;

public class InvalidWeaponException : Exception
{
    public InvalidWeaponException(string message) : base(message)
    {
    }

    public override string Message => "Invalid weapon. This character can't use it.";
}