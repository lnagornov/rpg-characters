namespace RpgCharacters.Exceptions;

public class InvalidCharacterException : Exception
{
    public InvalidCharacterException(string message) : base(message)
    {
    }

    public override string Message => "Invalid character type.";
}