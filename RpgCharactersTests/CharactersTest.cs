using System;
using System.IO;
using System.Text;
using RpgCharacters.Attributes;
using RpgCharacters.Characters;
using RpgCharacters.Exceptions;
using RpgCharacters.Items;
using Xunit;

namespace RpgCharactersTests;

public class CharactersTest
{
    #region Character can be created with type Mage, Rogue, Ranger, Warrior

    [Fact]
    public void CreateCharacter_CreateCharacterMage_ShouldReturnCharacterWithTypeMage()
    {
        // Arrange
        Character newCharacterMage = CharacterFactory.CreateCharacter(CharacterType.Mage);

        // Assert
        Assert.IsType<Mage>(newCharacterMage);
    }

    [Fact]
    public void CreateCharacter_CreateCharacterRogue_ShouldReturnCharacterWithTypeRogue()
    {
        // Arrange
        Character newCharacterRogue = CharacterFactory.CreateCharacter(CharacterType.Rogue);

        // Assert
        Assert.IsType<Rogue>(newCharacterRogue);
    }

    [Fact]
    public void CreateCharacter_CreateCharacterRanger_ShouldReturnCharacterWithTypeRanger()
    {
        // Arrange
        Character newCharacterRanger = CharacterFactory.CreateCharacter(CharacterType.Ranger);

        // Assert
        Assert.IsType<Ranger>(newCharacterRanger);
    }

    [Fact]
    public void CreateCharacter_CreateCharacterWarrior_ShouldReturnCharacterWithTypeWarrior()
    {
        // Arrange
        Character newCharacterWarrior = CharacterFactory.CreateCharacter(CharacterType.Warrior);

        // Assert
        Assert.IsType<Warrior>(newCharacterWarrior);
    }

    #endregion

    #region Character has level 1 when created

    [Theory]
    [InlineData(CharacterType.Mage)]
    [InlineData(CharacterType.Ranger)]
    [InlineData(CharacterType.Rogue)]
    [InlineData(CharacterType.Warrior)]
    public void Constructor_CharacterCreated_CharacterShouldBeLevel1WhenCreated(CharacterType characterType)
    {
        // Arrange
        int characterLevelWhenCreated = 1;
        int expected = characterLevelWhenCreated;

        // Act
        Character character = CharacterFactory.CreateCharacter(characterType);
        int actual = character.Level;

        // Assert
        Assert.Equal(expected, actual);
    }

    #endregion

    #region Character base primary attributes setting

    [Fact]
    public void
        SetBasePrimaryAttributes_SetCustomBasePrimaryAttributesToCharacter_ShouldSetBasePrimaryAttributesToCharacter()
    {
        // Arrange
        Character rouge = CharacterFactory.CreateCharacter(CharacterType.Rogue);
        PrimaryAttributes baseAttributes = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Rogue);
        PrimaryAttributes expected = baseAttributes;

        // Act
        rouge.BasePrimaryAttributes = baseAttributes;
        PrimaryAttributes actual = rouge.BasePrimaryAttributes;

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    #endregion

    #region Character has base primary attributes when created

    [Theory]
    [InlineData(CharacterType.Mage)]
    [InlineData(CharacterType.Ranger)]
    [InlineData(CharacterType.Rogue)]
    [InlineData(CharacterType.Warrior)]
    public void Constructor_CharacterCreated_ShouldHaveBasePrimaryAttributesForItsType(CharacterType characterType)
    {
        // Arrange
        PrimaryAttributes basePrimaryAttributes = PrimaryAttributesFactory.GetBaseAttributes(characterType);
        PrimaryAttributes expected = basePrimaryAttributes;

        // Act
        Character newCharacter = CharacterFactory.CreateCharacter(characterType);
        PrimaryAttributes actual = newCharacter.BasePrimaryAttributes;

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    #endregion

    #region Character can level up

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public void LevelUp_IncreaseCharactersLevelWithPositiveInteger_ShouldIncreaseCharactersLevel(int levelsToUp)
    {
        // Arrange
        Character ranger = CharacterFactory.CreateCharacter(CharacterType.Ranger);
        int CHARACTER_START_LEVEL = 1;
        int expected = levelsToUp + CHARACTER_START_LEVEL;

        // Act
        ranger.LevelUp(levelsToUp);
        int actual = ranger.Level;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(-1)]
    public void LevelUp_IncreaseCharactersLevelWithZeroOrNegativeInteger_ShouldThrowInvalidCharacterLevelException(
        int levelsToUp)
    {
        // Arrange
        Character mage = CharacterFactory.CreateCharacter(CharacterType.Mage);

        // Act and Assert
        Assert.Throws<InvalidCharacterLevelException>(() => { mage.LevelUp(levelsToUp); });
    }

    #endregion

    #region Character name setting

    [Fact]
    public void SetName_SetCustomCharacterName_ShouldSetCharacterName()
    {
        // Arrange 
        Character mage = CharacterFactory.CreateCharacter(CharacterType.Mage);
        string NAME_GANDALF = "Gandalf";
        string expected = NAME_GANDALF;

        // Act
        mage.Name = "Gandalf";
        string actual = mage.Name;

        // Assert
        Assert.Equal(expected, actual);
    }

    #endregion

    #region Character attributes increase when level up

    [Theory]
    [InlineData(CharacterType.Mage)]
    [InlineData(CharacterType.Ranger)]
    [InlineData(CharacterType.Rogue)]
    [InlineData(CharacterType.Warrior)]
    public void
        LevelUp_CharacterLevelUpBy1Level_ShouldIncreaseCharacterBasePrimaryAttributesAccordingToCharacterTypeAnd1LevelUp(
            CharacterType characterType)
    {
        // Arrange
        Character character = CharacterFactory.CreateCharacter(characterType);
        PrimaryAttributes expected = PrimaryAttributesFactory.GetBaseAttributes(characterType) +
                                     PrimaryAttributesFactory.GetLevelUpAttributes(characterType);
        // Act
        character.LevelUp();
        PrimaryAttributes actual = character.BasePrimaryAttributes;

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    [Theory]
    [InlineData(2)]
    [InlineData(3)]
    public void
        LevelUp_MageLevelUpBySeveralLevels_ShouldIncreaseCharacterBasePrimaryAttributesAccordingToCharacterTypeAndSeveralLevelUps(
            int levels)
    {
        // Arrange
        Character mage = CharacterFactory.CreateCharacter(CharacterType.Mage);
        PrimaryAttributes expected = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Mage) +
                                     PrimaryAttributesFactory.GetLevelUpAttributes(CharacterType.Mage) * levels;
        // Act
        mage.LevelUp(levels);
        PrimaryAttributes actual = mage.BasePrimaryAttributes;

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    #endregion

    #region Character total attributes calculation

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    public void
        GetTotalPrimaryAttributes_GetTotalPrimaryAttributesOfMageWithDifferentLevel_ShouldReturnTotalAttributesAsSumOfBaseAndLevelUpAttributes(
            int levelsToUp)
    {
        // Arrange
        Character mage = CharacterFactory.CreateCharacter(CharacterType.Mage);
        PrimaryAttributes expected = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Mage) +
                                     PrimaryAttributesFactory.GetLevelUpAttributes(CharacterType.Mage) * levelsToUp;

        // Act
        mage.LevelUp(levelsToUp);
        PrimaryAttributes actual = mage.TotalPrimaryAttributes;

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    [Fact]
    public void
        GetTotalPrimaryAttributes_GetTotalPrimaryAttributesOfMageLevel2WithArmorEquipped_ShouldReturnTotalAttributesAsSumOfBasePlusLevelUpPlusArmorAttributes()
    {
        // Arrange
        Character mage = CharacterFactory.CreateCharacter(CharacterType.Mage);
        Armor armor = new Armor("Magic Hat", 1, SlotType.Head, ArmorType.Cloth,
            new PrimaryAttributes() {Strength = 0, Dexterity = 0, Intelligence = 10});
        PrimaryAttributes expected = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Mage) +
                                     PrimaryAttributesFactory.GetLevelUpAttributes(CharacterType.Mage) +
                                     armor.Attributes;

        // Act
        mage.LevelUp();
        mage.EquipArmor(armor);
        PrimaryAttributes actual = mage.TotalPrimaryAttributes;

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    #endregion

    #region Character can't equip unfamilliar weapon type

    [Theory]
    [InlineData("Axe", 1, SlotType.Weapon, WeaponType.Axe, 1, 1.0)]
    [InlineData("Bow", 1, SlotType.Weapon, WeaponType.Bow, 1, 1.0)]
    [InlineData("Dagger", 1, SlotType.Weapon, WeaponType.Dagger, 1, 1.0)]
    [InlineData("Hammer", 1, SlotType.Weapon, WeaponType.Hammer, 1, 1.0)]
    [InlineData("Sword", 1, SlotType.Weapon, WeaponType.Sword, 1, 1.0)]
    public void EquipWeapon_EquipNotAllowedWeaponTypeByMage_ShouldThrowInvalidWeaponException(string name,
        int levelRequired, SlotType slot, WeaponType type, int damage, double attackSpeed)
    {
        // Arrange 
        Mage mage = new Mage();
        Weapon weapon = new Weapon(name, levelRequired, slot, type, damage, attackSpeed);

        // Act and Assert
        Assert.Throws<InvalidWeaponException>(() => { mage.EquipWeapon(weapon); });
    }

    [Theory]
    [InlineData("Axe", 1, SlotType.Weapon, WeaponType.Axe, 1, 1.0)]
    [InlineData("Dagger", 1, SlotType.Weapon, WeaponType.Dagger, 1, 1.0)]
    [InlineData("Hammer", 1, SlotType.Weapon, WeaponType.Hammer, 1, 1.0)]
    [InlineData("Staff", 1, SlotType.Weapon, WeaponType.Staff, 1, 1.0)]
    [InlineData("Sword", 1, SlotType.Weapon, WeaponType.Sword, 1, 1.0)]
    [InlineData("Wand", 1, SlotType.Weapon, WeaponType.Wand, 1, 1.0)]
    public void EquipWeapon_EquipNotAllowedWeaponTypeByRanger_ShouldThrowInvalidWeaponException(string name,
        int levelRequired, SlotType slot, WeaponType type, int damage, double attackSpeed)
    {
        // Arrange 
        Ranger ranger = new Ranger();
        Weapon weapon = new Weapon(name, levelRequired, slot, type, damage, attackSpeed);

        // Act and Assert
        Assert.Throws<InvalidWeaponException>(() => { ranger.EquipWeapon(weapon); });
    }

    [Theory]
    [InlineData("Axe", 1, SlotType.Weapon, WeaponType.Axe, 1, 1.0)]
    [InlineData("Bow", 1, SlotType.Weapon, WeaponType.Bow, 1, 1.0)]
    [InlineData("Hammer", 1, SlotType.Weapon, WeaponType.Hammer, 1, 1.0)]
    [InlineData("Staff", 1, SlotType.Weapon, WeaponType.Staff, 1, 1.0)]
    [InlineData("Wand", 1, SlotType.Weapon, WeaponType.Wand, 1, 1.0)]
    public void EquipWeapon_EquipNotAllowedWeaponTypeByRogue_ShouldThrowInvalidWeaponException(string name,
        int levelRequired, SlotType slot, WeaponType type, int damage, double attackSpeed)
    {
        // Arrange 
        Rogue rogue = new Rogue();
        Weapon weapon = new Weapon(name, levelRequired, slot, type, damage, attackSpeed);

        // Act and Assert
        Assert.Throws<InvalidWeaponException>(() => { rogue.EquipWeapon(weapon); });
    }

    [Theory]
    [InlineData("Bow", 1, SlotType.Weapon, WeaponType.Bow, 1, 1.0)]
    [InlineData("Dagger", 1, SlotType.Weapon, WeaponType.Dagger, 1, 1.0)]
    [InlineData("Staff", 1, SlotType.Weapon, WeaponType.Staff, 1, 1.0)]
    [InlineData("Wand", 1, SlotType.Weapon, WeaponType.Wand, 1, 1.0)]
    public void EquipWeapon_EquipNotAllowedWeaponTypeByWarrior_ShouldThrowInvalidWeaponException(string name,
        int levelRequired, SlotType slot, WeaponType type, int damage, double attackSpeed)
    {
        // Arrange 
        Warrior warrior = new Warrior();
        Weapon weapon = new Weapon(name, levelRequired, slot, type, damage, attackSpeed);

        // Act and Assert
        Assert.Throws<InvalidWeaponException>(() => { warrior.EquipWeapon(weapon); });
    }

    #endregion

    #region Character can't equip unfamilliar armor type

    [Theory]
    [InlineData("Some Armor", 1, SlotType.Head, ArmorType.Leather)]
    [InlineData("Some Armor", 1, SlotType.Body, ArmorType.Mail)]
    [InlineData("Some Armor", 1, SlotType.Legs, ArmorType.Plate)]
    public void EquipArmor_EquipNotAllowedArmorTypeByMage_ShouldThrowInvalidArmorException(string name,
        int levelRequired, SlotType slot, ArmorType type)
    {
        // Arrange 
        PrimaryAttributes attributes = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 1};
        Armor armor = new Armor(name, levelRequired, slot, type, attributes);
        Mage mage = new Mage();

        // Act and Assert
        Assert.Throws<InvalidArmorException>(() => { mage.EquipArmor(armor); });
    }

    [Theory]
    [InlineData("Some Armor", 1, SlotType.Body, ArmorType.Cloth)]
    [InlineData("Some Armor", 1, SlotType.Legs, ArmorType.Plate)]
    public void EquipArmor_EquipNotAllowedArmorTypeByRanger_ShouldThrowInvalidArmorException(string name,
        int levelRequired, SlotType slot, ArmorType type)
    {
        // Arrange 
        PrimaryAttributes attributes = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 1};
        Armor armor = new Armor(name, levelRequired, slot, type, attributes);
        Ranger ranger = new Ranger();

        // Act and Assert
        Assert.Throws<InvalidArmorException>(() => { ranger.EquipArmor(armor); });
    }

    [Theory]
    [InlineData("Some Armor", 1, SlotType.Body, ArmorType.Cloth)]
    [InlineData("Some Armor", 1, SlotType.Legs, ArmorType.Plate)]
    public void EquipArmor_EquipNotAllowedArmorTypeByRogue_ShouldThrowInvalidArmorException(string name,
        int levelRequired, SlotType slot, ArmorType type)
    {
        // Arrange 
        PrimaryAttributes attributes = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 1};
        Armor armor = new Armor(name, levelRequired, slot, type, attributes);
        Rogue rogue = new Rogue();

        // Act and Assert
        Assert.Throws<InvalidArmorException>(() => { rogue.EquipArmor(armor); });
    }

    [Theory]
    [InlineData("Some Armor", 1, SlotType.Head, ArmorType.Cloth)]
    [InlineData("Some Armor", 1, SlotType.Body, ArmorType.Leather)]
    public void EquipArmor_EquipNotAllowedArmorTypeByWarrior_ShouldThrowInvalidArmorException(string name,
        int levelRequired, SlotType slot, ArmorType type)
    {
        // Arrange 
        PrimaryAttributes attributes = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 1};
        Armor armor = new Armor(name, levelRequired, slot, type, attributes);
        Warrior warrior = new Warrior();

        // Act and Assert
        Assert.Throws<InvalidArmorException>(() => { warrior.EquipArmor(armor); });
    }

    #endregion

    #region Character can equip armor

    [Theory]
    [InlineData("Cloth Head Armor", 1, SlotType.Head, ArmorType.Cloth)]
    [InlineData("Cloth Body Armor", 1, SlotType.Body, ArmorType.Cloth)]
    [InlineData("Cloth Legs Armor", 1, SlotType.Legs, ArmorType.Cloth)]
    public void EquipArmor_EquipArmorIntoFreeSlotByMage_ShouldReturnSuccessMessage(string name,
        int levelRequired, SlotType slot, ArmorType type)
    {
        // Arrange
        PrimaryAttributes attributes = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 1};
        Armor armor = new Armor(name, levelRequired, slot, type, attributes);
        Character mage = new Mage();
        string successMessage = "New armor equipped!";
        string expected = successMessage;

        // Act
        string actual = mage.EquipArmor(armor);

        // Assert
        Assert.Equal(expected, actual);
    }
    
    [Theory]
    [InlineData("Cloth Head Armor", 1, SlotType.Head, ArmorType.Cloth)]
    [InlineData("Cloth Body Armor", 1, SlotType.Body, ArmorType.Cloth)]
    [InlineData("Cloth Legs Armor", 1, SlotType.Legs, ArmorType.Cloth)]
    public void EquipArmor_EquipArmorIntoFreeSlotByMage_ShouldAddArmorToCharacterEquipmentToCertainSlot(string name,
        int levelRequired, SlotType slot, ArmorType type)
    {
        // Arrange
        PrimaryAttributes attributes = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 1};
        Armor armor = new Armor(name, levelRequired, slot, type, attributes);
        Character mage = new Mage();
        Armor expected = armor;

        // Act
        mage.EquipArmor(armor);
        Item? actual = mage.Equipment[slot];

        // Assert
        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData("Leather Head Armor", 1, SlotType.Head, ArmorType.Leather)]
    [InlineData("Leather Body Armor", 1, SlotType.Body, ArmorType.Leather)]
    [InlineData("Leather Legs Armor", 1, SlotType.Legs, ArmorType.Leather)]
    public void EquipArmor_EquipArmorIntoFreeSlotByRanger_ShouldAddArmorToCharacterEquipmentToCertainSlot(string name,
        int levelRequired, SlotType slot, ArmorType type)
    {
        // Arrange
        PrimaryAttributes attributes = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 1};
        Armor armor = new Armor(name, levelRequired, slot, type, attributes);
        Character ranger = new Ranger();
        Armor expected = armor;

        // Act
        ranger.EquipArmor(armor);
        Item? actual = ranger.Equipment[slot];

        // Assert
        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData("Leather Head Armor", 1, SlotType.Head, ArmorType.Leather)]
    [InlineData("Leather Body Armor", 1, SlotType.Body, ArmorType.Leather)]
    [InlineData("Leather Legs Armor", 1, SlotType.Legs, ArmorType.Leather)]
    public void EquipArmor_EquipArmorIntoFreeSlotByRogue_ShouldAddArmorToCharacterEquipmentToCertainSlot(string name,
        int levelRequired, SlotType slot, ArmorType type)
    {
        // Arrange
        PrimaryAttributes attributes = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 1};
        Armor armor = new Armor(name, levelRequired, slot, type, attributes);
        Character rogue = new Rogue();
        Armor expected = armor;

        // Act
        rogue.EquipArmor(armor);
        Item? actual = rogue.Equipment[slot];

        // Assert
        Assert.Equal(expected, actual);
    }

    [Theory]
    [InlineData("Mail Head Armor", 1, SlotType.Head, ArmorType.Mail)]
    [InlineData("Mail Body Armor", 1, SlotType.Body, ArmorType.Mail)]
    [InlineData("Mail Legs Armor", 1, SlotType.Legs, ArmorType.Mail)]
    public void EquipArmor_EquipArmorIntoFreeSlotByWarrior_ShouldAddArmorToCharacterEquipmentToCertainSlot(string name,
        int levelRequired, SlotType slot, ArmorType type)
    {
        // Arrange
        PrimaryAttributes attributes = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 1};
        Armor armor = new Armor(name, levelRequired, slot, type, attributes);
        Character warrior = new Warrior();
        Armor expected = armor;

        // Act
        warrior.EquipArmor(armor);
        Item? actual = warrior.Equipment[slot];

        // Assert
        Assert.Equal(expected, actual);
    }

    #endregion

    #region Character can equip weapon

    [Theory]
    [InlineData("Magic Staff", 1, SlotType.Weapon, WeaponType.Staff, 1, 1.0)]
    public void EquipWeapon_EquipWeaponIntoFreeSlotByMage_ShouldReturnSuccessMessage(string name,
        int levelRequired, SlotType slot, WeaponType type, int damage, double attackSpeed)
    {
        // Arrange
        Weapon weapon = new Weapon(name, levelRequired, slot, type, damage, attackSpeed);
        Character mage = new Mage();
        string successMessage = "New weapon equipped!";
        string expected = successMessage;

        // Act
        string actual = mage.EquipWeapon(weapon);

        // Assert
        Assert.Equal(expected, actual);
    }
    
    [Theory]
    [InlineData("Magic Staff", 1, SlotType.Weapon, WeaponType.Staff, 1, 1.0)]
    public void EquipWeapon_EquipWeaponIntoFreeSlotByMage_ShouldAddWeaponToCharacterEquipmentToWeaponSlot(string name,
        int levelRequired, SlotType slot, WeaponType type, int damage, double attackSpeed)
    {
        // Arrange
        Weapon weapon = new Weapon(name, levelRequired, slot, type, damage, attackSpeed);
        Character mage = new Mage();
        Weapon expected = weapon;

        // Act
        mage.EquipWeapon(weapon);
        Item? actual = mage.Equipment[slot];

        // Assert
        Assert.Equal(expected, actual);
    }

    #endregion

    #region Character can't equip high level armor

    [Theory]
    [InlineData("Mail Head Armor", 2, SlotType.Head, ArmorType.Mail)]
    public void EquipArmor_EquipHighLevelArmorIntoFreeSlotByWarrior_ShouldThrowInvalidCharacterLevelException(
        string name,
        int levelRequired, SlotType slot, ArmorType type)
    {
        // Arrange
        PrimaryAttributes attributes = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 1};
        Armor highLevelArmor = new Armor(name, levelRequired, slot, type, attributes);
        Character warrior = new Warrior();

        // Act and Assert
        Assert.Throws<InvalidCharacterLevelException>(() => { warrior.EquipArmor(highLevelArmor); });
    }

    #endregion

    #region Character can't equip high level weapon

    [Theory]
    [InlineData("Elven Bow", 2, SlotType.Weapon, WeaponType.Bow, 1, 1.0)]
    public void EquipArmor_EquipHighLevelWeaponIntoFreeSlotByRanger_ShouldThrowInvalidCharacterLevelException(
        string name,
        int levelRequired, SlotType slot, WeaponType type, int damage, double attackSpeed)
    {
        // Arrange
        Weapon highLevelWeapon = new Weapon(name, levelRequired, slot, type, damage, attackSpeed);
        Character warrior = new Ranger();

        // Act and Assert
        Assert.Throws<InvalidCharacterLevelException>(() => { warrior.EquipWeapon(highLevelWeapon); });
    }

    #endregion

    #region Character stats can be displayed to console

    [Fact]
    public void
        DisplayStats_DisplayCharacterStatisticsOfWarriorLevel2WithArmorAndWeaponEquipped_ShouldPrintOutStatsToConsole()
    {
        // Arrange
        // --- Create warrior, weapon, armor ---
        Character warrior = CharacterFactory.CreateCharacter(CharacterType.Warrior);
        Weapon axe = new Weapon("Axe", 1, SlotType.Weapon, WeaponType.Axe, 7, 1.1);
        Armor plateArmor = new Armor("Plate armor", 1, SlotType.Body, ArmorType.Plate,
            new PrimaryAttributes() {Strength = 1});
        warrior.LevelUp();
        warrior.EquipWeapon(axe);
        warrior.EquipArmor(plateArmor);
        // --- Now warrior should be Level - 2, and be equipped with weapon and armor ---

        // --- Create strings of Statistics with String Builder ---
        StringBuilder warriorStats = new StringBuilder();
        warriorStats.AppendLine("Name: " + warrior.Name);
        warriorStats.AppendLine("Level: " + warrior.Level);
        warriorStats.AppendLine("Strength: " + warrior.TotalPrimaryAttributes.Strength);
        warriorStats.AppendLine("Dexterity: " + warrior.TotalPrimaryAttributes.Dexterity);
        warriorStats.AppendLine("Intelligence: " + warrior.TotalPrimaryAttributes.Intelligence);
        warriorStats.AppendLine("Damage: " + warrior.CalculateDamage());
        string expected = warriorStats + "\n";

        // --- Set console output to variable ---
        StringWriter stringWriter = new StringWriter();
        Console.SetOut(stringWriter);

        // Act
        warrior.DisplayStats();
        string actual = stringWriter.ToString();

        // Assert
        Assert.Equal(expected, actual);
    }

    #endregion

    #region Character damage calculation if no weapon is equipped

    [Fact]
    public void
        CalculateDamage_CalculateDamageByWarriorWithOutWeapon_ShouldReturnCalculatedDamageAmountDealtByWarriorWithOutWeapon()
    {
        // Arrange
        Character warrior = CharacterFactory.CreateCharacter(CharacterType.Warrior);
        int warriorStrengthAtLevel1 = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Warrior).Strength;
        double expected = Character.BaseDamage * (1 + (double) warriorStrengthAtLevel1 / 100);

        // Act
        double actual = warrior.CalculateDamage();

        // Assert
        Assert.Equal(expected, actual);
    }

    #endregion

    #region Character damage calculation with valid weapon equipped

    [Fact]
    public void
        CalculateDamage_CalculateDamageByWarriorWithValidWeaponEquipped_ShouldReturnCalculatedDamageAmountDealtByWarriorWithValidWeaponEquipped()
    {
        // Arrange
        Character warrior = CharacterFactory.CreateCharacter(CharacterType.Warrior);
        Weapon axe = new Weapon("Axe", 1, SlotType.Weapon, WeaponType.Axe, 7, 1.1);
        int warriorStrengthAtLevel1 = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Warrior).Strength;
        double weaponDamagePerSecond = axe.Damage * axe.AttackSpeed;
        double expected = weaponDamagePerSecond * (1 + (double) warriorStrengthAtLevel1 / 100);

        // Act
        warrior.EquipWeapon(axe);
        double actual = warrior.CalculateDamage();

        // Assert
        Assert.Equal(expected, actual);
    }

    #endregion

    #region Character damage calculation with valid weapon and armor equipped

    [Fact]
    public void
        CalculateDamage_CalculateDamageByWarriorWithValidWeaponAndArmorEquipped_ShouldReturnCalculatedDamageAmountDealtByWarriorWithValidWeaponAndArmorEquipped()
    {
        // Arrange
        Character warrior = CharacterFactory.CreateCharacter(CharacterType.Warrior);
        Weapon axe = new Weapon("Axe", 1, SlotType.Weapon, WeaponType.Axe, 7, 1.1);
        Armor plateArmor = new Armor("Plate armor", 1, SlotType.Body, ArmorType.Plate,
            new PrimaryAttributes() {Strength = 1});
        int warriorStrengthAtLevel1 = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Warrior).Strength;
        int totalPrimaryAttribute = warriorStrengthAtLevel1 + plateArmor.Attributes.Strength;
        double weaponDamagePerSecond = axe.Damage * axe.AttackSpeed;
        double expected = weaponDamagePerSecond * (1 + (double) totalPrimaryAttribute / 100);

        // Act
        warrior.EquipWeapon(axe);
        warrior.EquipArmor(plateArmor);
        double actual = warrior.CalculateDamage();

        // Assert
        Assert.Equal(expected, actual);
    }

    #endregion
}