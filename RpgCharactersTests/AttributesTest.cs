using RpgCharacters.Attributes;
using RpgCharacters.Characters;
using Xunit;

namespace RpgCharactersTests;

public class AttributesTest
{
    #region Primary attributes addition

    [Fact]
    public void
        OperatorPlus_AddTwoDifferentPrimaryAttributes_ShouldReturnNewPrimaryAttributesWithSumOfEachAttributeRespectively()
    {
        // Arrange
        PrimaryAttributes primaryAttributes111 = new PrimaryAttributes()
            {Strength = 1, Dexterity = 1, Intelligence = 1};
        PrimaryAttributes primaryAttributes234 = new PrimaryAttributes()
            {Strength = 2, Dexterity = 3, Intelligence = 4};
        PrimaryAttributes expected = new PrimaryAttributes {Strength = 3, Dexterity = 4, Intelligence = 5};

        // Act
        PrimaryAttributes actual = primaryAttributes111 + primaryAttributes234;

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    #endregion
    
    #region Primary attributes multiplication by an integer

    [Fact]
    public void OperatorMultiply_MultiplyPrimaryAttributesBy3_ShouldReturnNewMultipliedAttributesBy3()
    {
        // Arrange
        PrimaryAttributes primaryAttributes111 = new PrimaryAttributes()
            {Strength = 1, Dexterity = 1, Intelligence = 1};
        int multiplier = 3;
        PrimaryAttributes expected = new PrimaryAttributes {Strength = 3, Dexterity = 3, Intelligence = 3};

        // Act
        PrimaryAttributes actual = primaryAttributes111 * multiplier;

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    #endregion

    #region Base attributes creation for each character type

    [Fact]
    public void GetBaseAttributes_GetBaseAttributesForMage_ShouldReturnBaseAttributesForMage()
    {
        // Arrange
        PrimaryAttributes expected = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 8};

        // Act
        PrimaryAttributes actual = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Mage);

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    [Fact]
    public void GetBaseAttributes_GetBaseAttributesForRanger_ShouldReturnBaseAttributesForRanger()
    {
        // Arrange
        PrimaryAttributes expected = new PrimaryAttributes() {Strength = 1, Dexterity = 7, Intelligence = 1};

        // Act
        PrimaryAttributes actual = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Ranger);

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    [Fact]
    public void GetBaseAttributes_GetBaseAttributesForRogue_ShouldReturnBaseAttributesForRogue()
    {
        // Arrange
        PrimaryAttributes expected = new PrimaryAttributes() {Strength = 2, Dexterity = 6, Intelligence = 1};

        // Act
        PrimaryAttributes actual = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Rogue);

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    [Fact]
    public void GetBaseAttributes_GetBaseAttributesForWarrior_ShouldReturnBaseAttributesForWarrior()
    {
        // Arrange
        PrimaryAttributes expected = new PrimaryAttributes() {Strength = 5, Dexterity = 2, Intelligence = 1};

        // Act
        PrimaryAttributes actual = PrimaryAttributesFactory.GetBaseAttributes(CharacterType.Warrior);

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    #endregion

    #region Level up attributes creation for each character type

    [Fact]
    public void GetLevelUpAttributes_GetLevelUpAttributesForMage_ShouldReturnLevelUpAttributesForMage()
    {
        // Arrange
        PrimaryAttributes expected = new PrimaryAttributes() {Strength = 1, Dexterity = 1, Intelligence = 5};

        // Act
        PrimaryAttributes actual = PrimaryAttributesFactory.GetLevelUpAttributes(CharacterType.Mage);

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    [Fact]
    public void GetLevelUpAttributes_GetLevelUpAttributesForRanger_ShouldReturnGetLevelUpAttributesForRanger()
    {
        // Arrange
        PrimaryAttributes expected = new PrimaryAttributes() {Strength = 1, Dexterity = 5, Intelligence = 1};

        // Act
        PrimaryAttributes actual = PrimaryAttributesFactory.GetLevelUpAttributes(CharacterType.Ranger);

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    [Fact]
    public void GetLevelUpAttributes_GetLevelUpAttributesForRogue_ShouldReturnGetLevelUpAttributesForRogue()
    {
        // Arrange
        PrimaryAttributes expected = new PrimaryAttributes() {Strength = 1, Dexterity = 4, Intelligence = 1};

        // Act
        PrimaryAttributes actual = PrimaryAttributesFactory.GetLevelUpAttributes(CharacterType.Rogue);

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    [Fact]
    public void GetLevelUpAttributes_GetLevelUpAttributesForWarrior_ShouldReturnGetLevelUpAttributesForWarrior()
    {
        // Arrange
        PrimaryAttributes expected = new PrimaryAttributes() {Strength = 3, Dexterity = 2, Intelligence = 1};

        // Act
        PrimaryAttributes actual = PrimaryAttributesFactory.GetLevelUpAttributes(CharacterType.Warrior);

        // Assert
        Assert.Equal(expected.Strength, actual.Strength);
        Assert.Equal(expected.Dexterity, actual.Dexterity);
        Assert.Equal(expected.Intelligence, actual.Intelligence);
    }

    #endregion
}